from django.urls import path

from .views import NewsApi, NewsView

app_name = "news"

urlpatterns = [
    path("", NewsView.as_view(), name="list"),
    path("json/", NewsApi.as_view({"get": "list"}), name="json"),
]
