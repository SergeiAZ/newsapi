from django.db import models


class Source(models.Model):
    '''
    Using column named id in Django requires values to be unique, use name source_id instead
    '''
    source_id = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255)

    class Meta:
        unique_together = ("source_id", "name")


class News(models.Model):
    """
    According to newsapi examples, fields which could be null marked as null in the model
    """

    source = models.ForeignKey(Source,on_delete=models.CASCADE)
    author = models.CharField(max_length=255, null=True, blank=True)
    title = models.CharField(max_length=255)
    description = models.TextField()
    content = models.TextField(null=True, blank=True)
    published_at = models.DateTimeField()
    url = models.URLField(max_length=500,unique=True)
    url_to_image = models.URLField(max_length=500,null=True, blank=True)

    class Meta:
        ordering = ["-published_at"]
