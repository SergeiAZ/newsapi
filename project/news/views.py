from django.views.generic import ListView
from rest_framework.renderers import JSONRenderer
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from .models import News
from .serializers import NewsSerializer


class NewsView(ListView):
    queryset = News.objects.all()[:20]


class NewsApi(ListModelMixin, GenericViewSet):
    queryset = News.objects.all()[:100]
    serializer_class = NewsSerializer

