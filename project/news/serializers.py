from rest_framework import serializers

from .models import News, Source


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ["source_id", "name"]


class NewsSerializer(serializers.ModelSerializer):

    source = SourceSerializer()

    class Meta:
        model = News
        fields = [
            "source",
            "author",
            "title",
            "content",
            "description",
            "published_at",
            "url",
            "url_to_image",
            "source",
        ]
