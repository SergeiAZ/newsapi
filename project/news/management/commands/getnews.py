from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from newsapi import NewsApiClient
from newsapi.newsapi_exception import NewsAPIException

from news.forms import NewsForm
from news.models import Source


class Command(BaseCommand):
    help = "Get latest news from newsapi"

    def handle(self, *args, **options):
        newsapi = NewsApiClient(api_key=settings.NEWSAPI_KEY)
        try:
            result = newsapi.get_everything(q="japan",page_size=100)
            if result["status"] == "ok":
                for article in result["articles"]:
                    news_form = NewsForm(article)
                    if news_form.is_valid():
                        news = news_form.save(commit=False)
                        source, created = Source.objects.get_or_create(
                            source_id=article["source"]["id"],
                            name=article["source"]["name"],
                        )
                        news.source = source
                        news.save()
                    else:
                        self.stdout.write(
                            self.style.NOTICE(
                                article
                            )
                        )
                        self.stdout.write(
                            self.style.NOTICE(
                                "Invalid data  - {} ".format(
                                    news_form.errors.as_json()
                                )
                            )
                        )
                self.stdout.write(self.style.SUCCESS("Done"))
        except (NewsAPIException, KeyError) as exp:
            raise CommandError(exp)
