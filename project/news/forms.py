from django import forms

from .mixins import UnderscoreizeFormDataMixin
from .models import News


class NewsForm(UnderscoreizeFormDataMixin, forms.ModelForm):

    published_at = forms.DateTimeField(input_formats=["%Y-%m-%dT%H:%M:%SZ"])

    class Meta:
        model = News
        fields = [
            "author",
            "title",
            "description",
            "content",
            "published_at",
            "url",
            "url_to_image",
        ]

