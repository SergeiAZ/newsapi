from djangorestframework_camel_case.util import underscoreize


class UnderscoreizeFormDataMixin(object):
    """
    Transform received data camelCase keys to match underscore names of model
    """

    def __init__(self, data=None, *args, **kwargs):

        data = underscoreize(data)

        super(UnderscoreizeFormDataMixin, self).__init__(data, *args, **kwargs)
